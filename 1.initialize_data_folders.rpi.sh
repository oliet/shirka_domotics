#!/bin/bash

set +x
set -e

Home_DIR="/home/shirka"
cd ${Home_DIR}
mkdir -p ${Home_DIR}/shirka.domotics.data
mkdir -p ${Home_DIR}/shirka.domotics.data/portainer
mkdir -p ${Home_DIR}/shirka.domotics.data/nodered
mkdir -p ${Home_DIR}/shirka.domotics.data/grafana
mkdir -p ${Home_DIR}/shirka.domotics.data/grafana/var
mkdir -p ${Home_DIR}/shirka.domotics.data/grafana/var/lib
mkdir -p ${Home_DIR}/shirka.domotics.data/grafana/var/lib/grafana
mkdir -p ${Home_DIR}/shirka.domotics.data/mosquitto
mkdir -p ${Home_DIR}/shirka.domotics.data/mosquitto/config
mkdir -p ${Home_DIR}/shirka.domotics.data/mosquitto/data
mkdir -p ${Home_DIR}/shirka.domotics.data/mosquitto/log
mkdir -p ${Home_DIR}/shirka.domotics.data/influxdb
mkdir -p ${Home_DIR}/shirka.domotics.data/influxdb/etc
mkdir -p ${Home_DIR}/shirka.domotics.data/influxdb/etc/influxdb
cd ${Home_DIR}/shirka.domotics.data/
git init

cd ${Home_DIR}
mkdir -p ${Home_DIR}/shirka.domotics.db
mkdir -p ${Home_DIR}/shirka.domotics.db/influxdb
mkdir -p ${Home_DIR}/shirka.domotics.db/influxdb/var
mkdir -p ${Home_DIR}/shirka.domotics.db/influxdb/var/lib
mkdir -p ${Home_DIR}/shirka.domotics.db/influxdb/var/lib/influxdb
cd ${Home_DIR}

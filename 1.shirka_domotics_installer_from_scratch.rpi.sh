#!/bin/bash

# clone repo
cd /home/shirka
git clone https://gitlab.com/oliet/shirka_domotics.git

# create persistent data folders, ONLY THE FIRST TIME, OTHERWISE INFLUXDB BBDD AND GRAFANA BBDD WILL BE ERASED
cd shirka_domotics
chmod +x 1.initialize_data_folders.rpi.sh
./1.initialize_data_folders.rpi.sh

sudo docker run -itd -p 8000:8000 -p 9000:9000 --name=portainer   --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /home/shirka/shirka.domotics.data/portainer:/data portainer/portainer-ce


# provide permissions over the whole folder
# sudo chmod -R 777 /home/shirka

# install systemd service and enable it
#cd /home/shirka/shirka_domotics/install/rpi
#sudo chmod +x ./install_service.rpi.sh
#sudo ./install_service.rpi.sh
